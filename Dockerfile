FROM ubuntu:xenial
RUN apt-get update; apt-get upgrade -y;\
	apt-get install -y \
		build-essential \
		libtool \
		autotools-dev \
		autoconf \
		ccache \
		pkg-config \
		libssl-dev \
		libboost-all-dev \
		wget \
		bsdmainutils \
		libqrencode-dev \
		libqt4-dev \
		libprotobuf-dev \
		protobuf-compiler \
		git-core \
    software-properties-common \
		gdb \
		libevent-dev;\
	rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/*
#RUN add-apt-repository ppa:webupd8team/java
#RUN apt-get update
#RUN apt-get install -y oracle-java8-installer
COPY bdb.sh /tmp/
RUN sh /tmp/bdb.sh
RUN wget "http://miniupnp.tuxfamily.org/files/download.php?file=miniupnpc-1.6.tar.gz" -O miniupnpc-1.6.tar.gz;\
	tar -xzvf miniupnpc-1.6.tar.gz; \
	cd miniupnpc-1.6; \
	make; \
	make install; \
	cd /; \
	rm -rf /miniupnpc-1.6;rm miniupnpc-1.6.tar.gz
RUN mkdir -p /home/developer && \
    echo "developer:x:1000:1000:Developer,,,:/home/developer:/bin/bash" >> /etc/passwd && \
    echo "developer:x:1000:" >> /etc/group && \
    chown developer:developer -R /home/developer
ENV HOME /home/developer
RUN mkdir /bitcoin; chown -R developer:developer /bitcoin
WORKDIR /bitcoin

# Setup base system
RUN apt update
RUN apt install -y apt-utils ca-certificates wget curl git gcc g++ locales \
    python3 python3-dev python3-appdirs locales gettext-base python3-pip \
    build-essential libzmq3-dev build-essential
RUN pip3 install --upgrade pip
RUN dpkg-reconfigure -f noninteractive locales && \
    locale-gen en_US.UTF-8 && \
    /usr/sbin/update-locale LANG=en_US.UTF-8
ENV LC_ALL en_US.UTF-8
RUN curl -sL https://deb.nodesource.com/setup_8.x | bash -
RUN apt install -y nodejs

# Install the custom bitcoin binaries
RUN mkdir -p /bitcoin
WORKDIR /bitcoin
COPY ./release/hazama-bitcoin.tar.bz2 /bitcoin
RUN tar xvf hazama-bitcoin.tar.bz2
RUN cp -rf /bitcoin/x86_64-pc-linux-gnu/* /usr

# Install indexd (required for counterparty)
RUN mkdir -p /indexd
WORKDIR /indexd
COPY ./release/indexd.tar.bz2 /indexd
RUN tar xvf indexd.tar.bz2
RUN npm install

# Install counterparty-(lib,cli)
WORKDIR /
COPY ./release/counterparty.tar.bz2 /
RUN tar xvf counterparty.tar.bz2

WORKDIR /counterparty-lib
RUN pip3 install -r requirements.txt
RUN python3 setup.py develop
RUN python3 setup.py install_apsw

WORKDIR /counterparty-cli
RUN pip3 install -r requirements.txt
RUN python3 setup.py develop

# Setup concurrently to handle binaries
WORKDIR /indexd
RUN npm install -g concurrently

# Copy the appropiate configuration files
RUN mkdir -p /home/developer/.bitcoin
COPY ./conf/bitcoin.conf /home/developer/.bitcoin/
RUN mkdir -p /home/developer/.config/counterparty
COPY ./conf/server.conf /home/developer/.config/counterparty/
COPY ./conf/client.conf /home/developer/.config/counterparty/
COPY ./conf/indexd.env /indexd/.env
RUN mkdir /data

# expose useful ports
EXPOSE 18444 18445 24000 28432 38000

CMD ["concurrently", "-k", "node index.js", "/usr/bin/bitcoind -conf=/home/developer/.bitcoin/bitcoin.conf", "/usr/local/bin/counterparty-server start"]
