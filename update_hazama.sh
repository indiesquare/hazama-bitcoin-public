#!/usr/bin/env bash

docker build -t hazama .
docker run -v `pwd`/data:/data -p "18444:18444" -p "18445:18445" -p "24000:24000" -p "28432:28432" -p "38000:38000" -i hazama
