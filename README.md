hazama-bitcoin-public:
=================

Public repository of the hazama bitcoin + counterparty deployment.

Quick oneliner:

    curl -sL https://bitbucket.org/indiesquare/hazama-bitcoin-public/downloads/deploy_hazama.sh | bash -

To install:

    git clone https://bitbucket.org/indiesquare/hazama-bitcoin-public.git hazama
    cd hazama
    docker build -t hazama .
    docker run -v `pwd`/data:/data -i hazama
